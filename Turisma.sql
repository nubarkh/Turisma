CREATE DATABASE Turisma;
GO

USE Turisma;


                                  --TABLES
CREATE TABLE Users(
   Id INT PRIMARY KEY IDENTITY(1,1),
   FirstName NVARCHAR(50) NOT NULL,
   LastName NVARCHAR(50) NOT NULL,
   Email NVARCHAR(50),
   FbAccount NVARCHAR(50) NULL
)

CREATE TABLE Countries(
   Id INT PRIMARY KEY IDENTITY(1,1),
   CountryName NVARCHAR(50) NOT NULL
)

CREATE TABLE Cities(
   Id INT PRIMARY KEY IDENTITY(1,1),
   IdCountry INT FOREIGN KEY REFERENCES Countries(Id) ON DELETE CASCADE,
   CityName NVARCHAR(50) NOT NULL
)

CREATE TABLE [Routes](
   Id INT PRIMARY KEY,
   IdUser INT FOREIGN KEY REFERENCES Users(Id),
   [Type] NVARCHAR(50) NOT NULL,
   IdCity INT FOREIGN KEY REFERENCES Cities(Id),
   [Time] TIME NOT NULL,
   Title NVARCHAR(50) NOT NULL,
   [Description] NVARCHAR(140) NOT NULL
)

CREATE TABLE [Route](
 Id INT PRIMARY KEY,
 IdRoute INT FOREIGN KEY REFERENCES [Routes](Id) ON DELETE CASCADE,
 Title NVARCHAR(50) NOT NULL,
 [Description] NVARCHAR(140) NOT NULL,
 Latitude NVARCHAR(20) NOT NULL,
 Longitude NVARCHAR(20) NOT NULL
)

CREATE TABLE Comments(
   Id INT PRIMARY KEY IDENTITY(1,1),
   IdUser INT  FOREIGN KEY REFERENCES [Users](Id) ON DELETE CASCADE,
   IdRoute INT  FOREIGN KEY REFERENCES [Routes](Id) ON DELETE CASCADE,
   Comment NVARCHAR(250) NOT NULL
)

CREATE TABLE Rating(
   Id INT PRIMARY KEY IDENTITY(1,1),
   IdUser INT  FOREIGN KEY REFERENCES [Users](Id) ON DELETE CASCADE,
   IdRoute INT  FOREIGN KEY REFERENCES [Routes](Id) ON DELETE CASCADE,
   RatingMark FLOAT NOT NULL
)

                                 --INSERTIONS

SET IDENTITY_INSERT Users OFF; 
BULK INSERT Users 
FROM 'C:\TempSQL\users.txt'
WITH(
    FORMATFILE='C:\TempSQL\users.fmt',
    CODEPAGE ='65001'
);

BULK INSERT [Routes] 
FROM 'C:\TempSQL\routes.txt'
WITH(
    FORMATFILE='C:\TempSQL\routes.fmt',
    CODEPAGE ='65001'
);

BULK INSERT [Route] 
FROM 'C:\TempSQL\route1.txt'
WITH(
    FORMATFILE='C:\TempSQL\route1.fmt',
    CODEPAGE ='65001'
);

INSERT INTO Countries
(CountryName)
VALUES
('Ukraine'),
('Azerbaijan');

INSERT INTO Cities
(IdCountry,CityName)
VALUES
(1,N'Odessa'),
(1,N'Kiev'),
(2,N'Baku');

 
INSERT INTO Rating
(IdRoute,IdUser,RatingMark)
VALUES
(1,1,5)


                                    -- CHANGES IN TABLES

--Adding new column for location
ALTER TABLE [Route]
ADD [Location] GEOGRAPHY;

--Converting latitude and longitude to geographic coordinates
UPDATE [Route] 
SET [Location]=geography::STGeomFromText(
    'POINT(' + Longitude+ ' ' + Latitude + ')', 4326)

	                            -- THE FIRST BACKUP  (FULL)
BACKUP DATABASE [Turisma]
TO DISK='C:\TempSQL\Turisma.bak'
WITH NAME='First Turisma Backup';


                            -- FUNCTIONS AND SP's

--1. All routes in the city which is asked by user.
GO
CREATE PROCEDURE sp_RoutesOfCitiesByCityName
   @cityName NVARCHAR(50)
AS   
   SELECT [Routes].Title, [Routes].[Description]
   FROM [Routes]
   WHERE Id=ANY( SELECT Id
			     FROM Cities
			     WHERE CityName=@cityName
				)
	 
--2.All routes and types in the city which is asked by user.
GO
CREATE FUNCTION RoutesOfCityByTypeAndCityName(
      @cityName NVARCHAR(50),
	  @type NVARCHAR(50)
)RETURNS TABLE
AS
RETURN(
  SELECT [Routes].Title, [Routes].[Description]
  FROM [Routes]
  WHERE Id=ANY(SELECT Id
			   FROM Cities
			   WHERE CityName=@cityName	 
	          ) AND [Type]=@type
)

--3.All routes and types in the city, sorting them by rating mark which is asked by user.
GO
CREATE FUNCTION SortRoutesOfCityByTypeAndCityName(
   @cityName NVARCHAR(50),
   @type NVARCHAR(50)
)RETURNS TABLE
AS
RETURN(
   SELECT TOP 100 [Routes].Title, [Routes].[Description]
   FROM [Routes]
   INNER JOIN Rating ON Rating.Id=[Routes].Id
   WHERE [Routes].IdCity=ANY(SELECT Id
			                 FROM Cities
			                 WHERE CityName=@cityName	 
	                         ) AND [Type]=@type
   GROUP BY [Routes].Title, [Routes].[Description],RatingMark
   ORDER BY RatingMark ASC
 )


--4.All routes which are created by the user.
GO
CREATE PROCEDURE sp_RoutesByUser(
   @firstName NVARCHAR(50),
   @lastName  NVARCHAR(50)
)
AS
   SELECT [Routes].Title, [Routes].[Description]
   FROM Users
   INNER JOIN [Routes] ON [Routes].IdUser=Users.Id
   WHERE FirstName=@firstName AND LastName=@lastName


	                   -- THE SECOND BACKUP (DIFFERENTIAL)
BACKUP DATABASE Turisma
TO DISK = 'C:\TempSQL\Turisma.bak'
WITH DIFFERENTIAL,
NAME = N'Differential_Turisma_Backup';

--5.Description of certain route and information about it by ID.
GO
CREATE PROCEDURE sp_DescriptionOfRouteById(
   @IdRoute INT=0
)
AS
   SELECT [Routes].Title, [Routes].[Type],
          [Routes].[Description]     
   FROM [Routes]
   WHERE [Routes].Id=@IdRoute

--6. All points of route.
GO
CREATE PROCEDURE sp_StationsOfRoute
  @IdRoute INT=0
AS
  SELECT [Route].Title, [Route].[Description]
  FROM [Route]
  WHERE IdRoute=@IdRoute


--7. All points of route for map.
GO
CREATE PROCEDURE sp_StationsOfRouteForMap
  @IdRoute INT=0
AS
  SELECT [Route].Title,[Route].[Location]
  FROM [Route]
  WHERE IdRoute=@IdRoute
 

--8. All comments of route.
GO
CREATE FUNCTION CommentsForRoute(
    @IdRoute INT=0
)RETURNS TABLE 
 RETURN(
      SELECT * 
	  FROM Comments
	  WHERE IdRoute=@IdRoute
)

--9. Request for registering user via email.
SET IDENTITY_INSERT Users ON; 
GO
CREATE PROCEDURE sp_RegisterUserByEmail 
   @firstName NVARCHAR(50),
   @lastName NVARCHAR(50),
   @email NVARCHAR(50)
AS
  BEGIN TRAN SP_RegisterUserByEmail 
  DECLARE @Result BIT=0;
  SET @Result = (SELECT 
                 CASE  WHEN @email LIKE '[a-z,0-9,_,-]%@[a-z,0-9,_,-]%.[a-z][a-z]%' 
                 THEN 1
                 ELSE 0 
				 END)
     IF @Result=1 AND NOT EXISTS(SELECT * 
	                             FROM Users 
								 WHERE @email=Email)					  
	  BEGIN
	    DECLARE @IdUser INT=0;

        SELECT @IdUser = Id+1 FROM Users ORDER BY Id;

	    INSERT INTO Users
	    (Id,FirstName,LastName,Email,FbAccount)
        VALUES
        (@IdUser,@firstName,@lastName,@email,NULL)
		IF @@ERROR!=0
        BEGIN
	      PRINT 'ERROR WHILE REGISTERING USER BY EMAIL'
          ROLLBACK TRAN SP_RegisterUserByEmail
		  RETURN
	    END
  	 END
	 ELSE
	   PRINT 'Email is invalid or already exists'
  COMMIT TRAN SP_RegisterUserByEmail;


--10.Request for registering user via facebook.
SET IDENTITY_INSERT Users ON; 
 GO
CREATE PROCEDURE sp_RegisterUserByFBaccount 
   @firstName NVARCHAR(50),
   @lastName NVARCHAR(50),
   @fbAccount NVARCHAR(50)
AS
  BEGIN TRAN SP_RegisterUserByFBaccount  
  IF LEN(@fbAccount)>1				  
     BEGIN
	    DECLARE @IdUser INT=0;
        SELECT @IdUser = Id+1 FROM Users ORDER BY Id;

	    INSERT INTO Users
	    (Id,FirstName,LastName,Email,FbAccount)
        VALUES
        (@IdUser,@firstName,@lastName,NULL,@fbAccount)
		IF @@ERROR!=0
          BEGIN
	         PRINT 'ERROR WHILE REGISTERING USER BY FB ACCOUNT'
             ROLLBACK TRAN SP_RegisterUserByFBaccount
		     RETURN
	      END
  	 END
	 ELSE
	   PRINT 'FB ACCOUNT IS INVALID'
  COMMIT TRAN SP_RegisterUserByFBaccount;

--11.Request for inserting a new route.
GO
CREATE PROCEDURE sp_AddRoute(
   @firstName NVARCHAR(50),
   @lastName  NVARCHAR(50),
   @type NVARCHAR(50),
   @cityName NVARCHAR(50),
   @time  TIME,
   @title NVARCHAR(50),
   @description  NVARCHAR(140)
 ) 
AS 
  BEGIN TRAN SP_AddRoute;
  IF EXISTS(SELECT *
            FROM Users 
			WHERE FirstName=@firstName AND LastName=@lastName) 
	  AND EXISTS (SELECT *
	              FROM Cities 
				  WHERE CityName=@cityName)
     BEGIN
	   DECLARE @IdCity INT=0, @Id INT=0, @IdUser INT=0;

	   SELECT @IdUser=Id 
	   FROM Users
	   WHERE FirstName=@firstName AND LastName=@lastName;

	   SELECT @IdCity=Id 
	   FROM Cities
	   WHERE CityName=@cityName;

	   SELECT @Id = Id+1 FROM [Routes] ORDER BY Id;

	   INSERT INTO [Routes]
	   (Id,IdUser,[Type],IdCity,[Time],Title,[Description])
	   VALUES
	   (@Id,@IdUser,@type,@IdCity,@time,@title,@description);

	   IF @@ERROR!=0
         BEGIN
	       PRINT 'ERROR WHILE INSERTING A NEW ROUTE';
           ROLLBACK TRAN SP_AddRoute;
		   RETURN
	    END
	 END 	
  COMMIT TRAN  SP_AddRoute;

--12. Request for inserting a new point to the route.
GO
CREATE PROCEDURE sp_AddPointsToRoute(
    @IdRoute INT=0,
	@title NVARCHAR(50),
	@description NVARCHAR(140),
	@latitude NVARCHAR(20),
	@longtitude NVARCHAR(20)
)
AS
  BEGIN TRAN SP_AddPointsToRoute;
  IF EXISTS(SELECT * FROM [Routes] WHERE Id=@IdRoute) 
    BEGIN
	  DECLARE @Id INT=0;
	  SELECT @Id = Id+1 FROM [Route] ORDER BY Id;

	  INSERT INTO [Route]
	  (Id,IdRoute,Title,[Description],Longitude,Latitude,[Location])
	  VALUES
	  (@Id,@IdRoute,@title,@description,@longtitude,@latitude,  
	  geography::STGeomFromText('POINT(' + @longtitude+ ' ' + @latitude + ')', 4326))		
	  IF @@ERROR!=0
        BEGIN
	      PRINT 'ERROR WHILE INSERTING A NEW Point to Route';
          ROLLBACK TRAN SP_AddPointsToRoute;
		  RETURN;
	    END
	 END
  COMMIT TRAN SP_AddPointsToRoute


--13. Leave a comment for route.
GO
CREATE PROCEDURE sp_AddCommentForRoute(
  @comment NVARCHAR(250),
  @IdRoute INT=0,
  @firstName NVARCHAR(50),
  @lastName NVARCHAR(50)
)
AS 
  BEGIN TRAN SP_AddCommentForRoute;
  IF EXISTS(SELECT * 
            FROM [Routes]
		    WHERE Id=@IdRoute) 
     AND EXISTS(SELECT * 
	            FROM Users
			    WHERE FirstName=@firstName AND LastName=@lastName)
    BEGIN
      DECLARE @IdUser INT=0;
	  SET @IdUser=(SELECT Id
	               FROM Users 
				   WHERE FirstName=@firstName AND LastName=@lastName);	

      INSERT INTO Comments
	  (IdRoute,IdUser,Comment)
	  VALUES
	  (@IdRoute,@IdUser,@comment)
	  IF @@ERROR!=0
        BEGIN
	       PRINT 'ERROR WHILE INSERTING A NEW COMMENT FOR Route';
           ROLLBACK TRAN SP_AddCommentForRoute;
		   RETURN
	    END
	 END
  COMMIT TRAN SP_AddCommentForRoute

 --14. Rate the route.
GO
CREATE PROCEDURE sp_RateRoute(
  @mark INT=0,
  @IdRoute INT=0,
  @firstName NVARCHAR(50),
  @lastName NVARCHAR(50)
)
AS 
  BEGIN TRAN SP_RateRoute;
  IF EXISTS(SELECT * 
            FROM [Routes] 
			WHERE Id=@IdRoute) 
     AND EXISTS(SELECT *
	            FROM Users 
				WHERE FirstName=@firstName AND LastName=@lastName)
    BEGIN
      DECLARE @IdUser INT=0;
	  SET @IdUser=(SELECT Id 
	               FROM Users
				   WHERE FirstName=@firstName AND LastName=@lastName);	

      INSERT INTO Rating
	  (IdRoute,IdUser,RatingMark)
	  VALUES
	  (@IdRoute,@IdUser,@mark)	  
	  IF @@ERROR!=0
        BEGIN
	      PRINT 'ERROR WHILE RATE Route';
          ROLLBACK TRAN SP_RateRoute;
		  RETURN;
	    END
	 END
  COMMIT TRAN SP_RateRoute

-- trigger for updating rating of route

GO
ALTER TABLE [Routes]
ADD Rating FLOAT not null DEFAULT 0;
GO
CREATE TRIGGER tr_RateRoute ON Rating
AFTER INSERT 
AS 
BEGIN
   DECLARE @IdRoute INT=0, @mark INT=0;
   SELECT @IdRoute=IdRoute, @mark=RatingMark
   FROM inserted  
   
   UPDATE [Routes]
   SET Rating=(Rating+@mark)/2
   WHERE Id=@IdRoute
END 

                                  --THE THIRD BACKUP (FULL)
BACKUP DATABASE [Turisma]
TO DISK='C:\TempSQL\Turisma.bak'
WITH NAME='Third Turisma Backup';